return {
	color = require(... .. ".color"),
	misc = require(... .. ".misc"),
	ui = require(... .. ".ui"),
}