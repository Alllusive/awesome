return {
	apps = {
		terminal = "kitty",
		editor = "emacsclient -a ''-c",
	},
	keys = {
		modkey = "Mod4",
		altkey = "Mod1",
	},
}
